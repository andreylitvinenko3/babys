<?php

namespace App\MainBundle\Pagination;

use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\Request;

class Pagination extends SlidingPagination
{
    private $defaultSortField;
    private $defaultSortDirection;

    public function __construct(Request $request, $count, $defaultLimit = 10, $defaultSortField = '_id', $defaultSortDirection = 'DESC')
    {
        $this->setCustomParameters([
            'sort_field' => $request->query->get('sort_field', $defaultSortField),
            'sort_direction' => strtolower($request->query->get('sort_direction', $defaultSortDirection))
        ]);
        $this->setPaginatorOptions([
            'sortFieldParameterName' => 'sort_field',
            'sortDirectionParameterName' => 'sort_direction',
            'pageParameterName' => 'page'
        ]);
        $this->setUsedRoute($request->attributes->get('_route'));
        $limit = (int)$request->query->get('limit');
        if ($limit <= 0) {
            $limit = $defaultLimit;
        }
        $page = max(1, (int)$request->query->get($this->getPaginatorOption('pageParameterName'), 1));

        $this->setItemNumberPerPage($limit);
        $this->setTotalItemCount($count);

        $this->setCurrentPageNumber($page);
        $this->setTemplate('MainBundle:Pagination:table_pagination.html.twig');
        $this->setSortableTemplate('MainBundle:Pagination:sortable_link.html.twig');


        parent::__construct(array_merge($request->attributes->get('_route_params'), [
            'sort_direction' => 'desc',
            'sort_field' => $defaultSortField,
        ], $request->query->all()));
        $this->defaultSortField = $defaultSortField;
        $this->defaultSortDirection = $defaultSortDirection;
    }

    public function getSortField()
    {
        return $this->customParameters[$this->getPaginatorOption('sortFieldParameterName')];
    }

    public function getSortDirection()
    {
        $direction = $this->customParameters[$this->getPaginatorOption('sortDirectionParameterName')];
        return strtoupper($direction) === 'ASC' ? 1 : -1;
    }

    public function getOffset()
    {
        return ($this->getCurrentPageNumber() - 1) * $this->getItemNumberPerPage();
    }


    public function count()
    {
        if ($this->getCurrentPageNumber() < $this->getMaxPageNumber()) {
            return $this->getItemNumberPerPage();
        }

        return $this->getItemNumberPerPage() - ($this->getMaxPageNumber() * $this->getItemNumberPerPage() - $this->getTotalItemCount());
    }

    public function getMaxPageNumber()
    {
        return ceil($this->getTotalItemCount() / $this->getItemNumberPerPage());
    }

    public function getDefaultSortField()
    {
        return $this->defaultSortField;
    }

    public function getDefaultSortDirection()
    {
        return $this->defaultSortDirection;
    }
}
