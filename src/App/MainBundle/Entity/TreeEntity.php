<?php

namespace App\MainBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class TreeEntity
{
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="`left`", type="integer")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="`right`", type="integer")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    public function getLevel()
    {
        return $this->level;
    }

    public function getRight()
    {
        return $this->right;
    }

    public function getLeft()
    {
        return $this->left;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    public function hasChildren()
    {
        return $this->right - $this->left > 1;
    }

    public function isParentFor(TreeEntity $node)
    {
        if ($node->getRoot() !== $this->root) {
            return false;
        }
        if ($this->left < $node->getLeft() && $this->right > $node->getRight()) {
            return true;
        }
        return false;
    }
}
