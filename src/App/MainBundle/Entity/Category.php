<?php

namespace App\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sirian\Helpers\TextUtils;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="categories", indexes={@ORM\Index(name="__idx_left_right_root", columns={"left", "right", "root"})})
 * @ORM\Entity(repositoryClass="App\MainBundle\Entity\Repository\CategoryRepository")
 * @Gedmo\Tree(type="nested")
 */
class Category extends TreeEntity
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title = '';

    /**
     * @var string
     * @ORM\Column(name="short_title", type="string", length=255)
     */
    private $shortTitle = '';

    /**
     * @var string
     * @ORM\Column(name="alias", type="string", length=255, unique=true)
     */
    private $alias = '';

    /**
     * @Gedmo\TreeParent
     * @var Category $parent
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     */
    private $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = TextUtils::slug($alias);
        return $this;
    }

    public function getShortTitle()
    {
        return $this->shortTitle;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;
        return $this;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    public function getPathName()
    {
        if ($this->parent) {
            return $this->parent->getPathName() . ' / ' .$this->shortTitle;
        }
        return $this->shortTitle;
    }

    public function isDirectChildOfParents(Category $category)
    {
        if (!$category->getParent()) {
            return false;
        }

        return ($category->getParent()->getId() == $this->getId() || $category->getParent()->isParentFor($this));
    }

    public function isParent($id)
    {
        if ($this->parent) {
            if ($this->parent->getId() == $id) {
                return true;
            } else {
                return $this->parent->isParent($id);
            }
        }

        return false;
    }

    public function getShiftedTitle()
    {
        return str_repeat(chr(194) . chr(160), $this->getLevel() * 4) . $this->getTitle();
    }
}
