<?php

namespace App\MainBundle\Entity\Repository;

use App\MainBundle\Filter\UserFilter;

class UserRepository extends EntityRepository
{
    public function createFilteredQueryBuilder(UserFilter $filter)
    {
        $qb = $this->createQueryBuilder('u');

        if ($filter->getRoles()) {
            $i = 0;
            foreach ($filter->getRoles() as $role) {
                $i++;
                $qb->andWhere('u.roles LIKE :role' . $i)->setParameter('role' . $i, '%' . $role  . '%');
            }
        }

        if ($filter->getEmail()) {
            $qb->andWhere('u.email LIKE :email')->setParameter('email', '%' . $filter->getEmail() . '%');
        }

        return $qb;
    }
}
