<?php

namespace App\MainBundle\Entity\Repository;

use App\MainBundle\Entity\Category;

class CategoryRepository extends TreeRepository
{
    public function getFakeParent()
    {
        $fakeParent = $this
            ->createQueryBuilder('node')
            ->andWhere('node.level = 0')
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (!$fakeParent) {
            $fakeParent = new Category();
            $fakeParent
                ->setTitle('fake root parent')
                ->setAlias('fake_root_parent')
                ->setLevel(0)
            ;

            $em = $this->getEntityManager();
            $em->persist($fakeParent);
            $em->flush();
        }

        return $fakeParent;
    }

    public function edit(Category $category)
    {
        $em = $this->getEntityManager();

        $parent = $category->getParent();

        if (!$parent) {
            $fakeParent = $this->getFakeParent();
            $category->setParent($fakeParent);
        }

        $em->persist($category);
        $em->flush();

        return true;
    }

    public function getTree($all = false)
    {
        $root = new Category();
        $root->setLevel(-1);

        $qb = $this->getNodesHierarchyQueryBuilder();

        if (!$all) {
            $qb->andWhere('node.level > 0');
        }

        $items = $qb->getQuery()->getResult();

        return $this->buildArrayTree($root, $items);
    }

    public function getTreeOf(array $items, $mergeFunction)
    {
        usort($items, function (Category $a, Category $b) {
            if ($a->getRoot() == $b->getRoot()) {
                return $a->getLeft() < $b->getLeft() ? -1 : 1;
            } else {
                return $a->getRoot()  < $b->getRoot() ? -1 : 1;
            }
        });

        $nodes = [];

        $currentNode = null;

        foreach ($items as $node) {
            $newNode = array_merge([
                'category' => $node,
                'children' => [],
                'level' => $node->getLevel(),
                'parent' => null,
            ], $mergeFunction($node));

            if ($node->getLevel() == 0) {
                $nodes[] = &$newNode;
                $currentNode = &$newNode;
            } else {
                while ($node->getLevel() <= $currentNode['level']) {
                    $currentNode = &$currentNode['parent'];
                }
                $newNode['parent'] = &$currentNode;
                $currentNode['children'][] = &$newNode;
                $currentNode = &$newNode;
            }

            unset($newNode);
        }

        return $nodes;
    }

    /**
     * @param $alias
     * @return Category
     */
    public function findOneByAlias($alias)
    {
        return $this->findOneBy([
            'alias' => $alias
        ]);
    }
}
