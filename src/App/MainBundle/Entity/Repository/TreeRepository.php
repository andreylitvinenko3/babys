<?php

namespace App\MainBundle\Entity\Repository;

use App\MainBundle\Entity\TreeEntity;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class TreeRepository extends NestedTreeRepository
{
    protected function buildArrayTree(TreeEntity $root, array $items)
    {
        $root = [
            'children' => [],
            'parent' => null,
            'level' => $root->getLevel(),
            'item' => $root
        ];
        $currentNode = &$root;

        /**
         * @var TreeEntity[] $items
         */
        foreach ($items as $item) {
            while ($item->getLevel() <= $currentNode['level']) {
                $currentNode = &$currentNode['parent'];
            }

            $node = [
                'item' => $item,
                'level' => $item->getLevel(),
                'children' => [],
                'parent' => &$currentNode,
            ];

            $currentNode['children'][] = &$node;

            if ($item->getLevel() > $currentNode['level']) {
                $currentNode = &$node;
            }
            unset($node);
        }

        return $root;
    }
}
