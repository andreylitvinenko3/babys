<?php

namespace App\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\MainBundle\Entity\Repository\ReviewRepository")
 * @ORM\Table(name="reviews")
 * @Vich\Uploadable
 */
class Review
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $text
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var string $text
     * @ORM\Column(type="string")
     */
    protected $author;

    /**
     * @var string $text
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @var bool $enabled
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * @Vich\UploadableField(mapping="review_image", fileNameProperty="imageName")
     * @Assert\Image
     *
     * @var File $imageFile
     */
    protected $imageFile;

    /**
     * @var string $imageName
     * @ORM\Column(name="image_name", type="string")
     */
    protected $imageName = '';

    /**
     * @var \DateTime $updatedAt
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = (int)$enabled;
        return $this;
    }

    public function isEnabled()
    {
        return (bool)$this->enabled;
    }

    public function getTitle()
    {
        return $this->getId() . ' ' . $this->getAuthor();
    }
}