<?php

namespace App\MainBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class EntityListener
{
    protected function recomputeSingleEntityChangeSet(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        $meta = $em->getClassMetadata(get_class($entity));
        $uow->recomputeSingleEntityChangeSet($meta, $entity);
    }

    protected function recomputeEntity(LifecycleEventArgs $eventArgs, $entity)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($entity));
        $uow->recomputeSingleEntityChangeSet($meta, $entity);
    }

    protected function computeEntity(LifecycleEventArgs $eventArgs, $entity)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($entity));
        $uow->computeChangeSet($meta, $entity);
    }
}
