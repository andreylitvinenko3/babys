<?php

namespace App\MainBundle\EventListener;

use App\MainBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class UserListener
{
    private $encoderFactory;

    public function __construct(EncoderFactory $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $user = $args->getEntity();
        if ($user instanceof User && $user->getPlainPassword()) {
            $this->updatePassword($user);
        }
    }

    public function preFlush(PreFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $identities = $uow->getIdentityMap();

        foreach ($identities as $key => $identity) {
            foreach ($identity as $id => $entity) {
                if ($entity instanceof User && $entity->getPlainPassword()) {
                    $this->updatePassword($entity);
                }
            }
        }
    }

    protected function updatePassword(User $user)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
        $user->setPlainPassword('');
    }
}

