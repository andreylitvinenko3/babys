<?php

namespace App\MainBundle\Controller;

use App\MainBundle\Service\ContainerTrait;
use App\MainBundle\Service\DoctrineTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class BaseController extends Controller
{
    use ContainerTrait;
    use DoctrineTrait;

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param $target
     * @param int $limit
     * @param array $options
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function paginate($target, $limit = 12, array $options = array())
    {
        $r = $this->getRequest();

        $pagination = $this->getPaginator()
            ->paginate(
                $target,
                $r->query->get('page', $r->request->get('page', 1)),
                $r->query->get('limit', $r->request->get('limit', $limit)),
                $options
            )
        ;

        return $pagination;
    }

    public function createNamedFormBuilder($name, $data = null, array $options = array())
    {
        return $this->getFormFactory()->createNamedBuilder($name, 'form', $data, $options);
    }

    public function createGETFormBuilder($data = null, array $options = array())
    {
        $builder = $this
            ->createFormBuilder($data, array_merge(['csrf_protection' => false], $options))
        ;

        $builder->setMethod('GET');

        return $builder;
    }
}