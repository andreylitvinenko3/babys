<?php

namespace App\MainBundle\Controller;

class DefaultController extends BaseController
{
    public function indexAction()
    {
        return $this->render('MainBundle:Default:index.html.twig');
    }
}
