<?php

namespace App\MainBundle\Controller;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;

class SecurityController extends BaseController
{
    public function loginAction(Request $request)
    {
        if ($this->getAuthorizationChecker()->isGranted('ROLE_USER')) {
            return $this->redirect($this->generateUrl('main'));
        }

        $authenticationUtils = $this->getAuthenticationUtils();
        $error = $authenticationUtils->getLastAuthenticationError();
        $username = $authenticationUtils->getLastUsername();

        $user = null;

        if ($username) {
            try {
                $user = $this->getUserProvider()->loadUserByUsername($username);
            } catch (\Exception $e) {}
        }

        $data = [
            '_username' => $username,
            '_password' => '',
            '_remember_me' => true
        ];

        $builder = $this
            ->getFormFactory()
            ->createNamedBuilder('', 'form', $data, [
                'csrf_protection' => true,
                'csrf_field_name' => '_csrf_token',
                'intention'       => 'authenticate'
            ])
        ;

        $builder
            ->add('_username', 'text', [
                'label' => 'Email',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('_password', 'password', [
                'label' => 'Пароль',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('_remember_me', 'hidden', [
                'data' => 1,
                'label' => 'Запомнить меня',
                'required' => false
            ])
            ->setAction($this->generateUrl('login_check'))
        ;

        $form = $builder->getForm();

        if ($error) {
            $form->addError(new FormError($error->getMessage()));
        }

        return $this->render('MainBundle:Security:login.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }
}