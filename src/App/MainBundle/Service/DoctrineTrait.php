<?php

namespace App\MainBundle\Service;

use App\MainBundle\Entity\Repository\CategoryRepository;
use App\MainBundle\Entity\Repository\ReviewRepository;
use App\MainBundle\Entity\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;

trait DoctrineTrait
{
    use ContainerAwareTrait;

    /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }

    /**
     * @param null $name
     * @return EntityManager
     */
    public function getEntityManager($name = null)
    {
        return $this->getDoctrine()->getManager($name);
    }

    /**
     * @param $name
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($name)
    {
        return $this->getDoctrine()->getRepository('MainBundle:' . $name);
    }

    /**
     * @return UserRepository
     */
    public function getUserRepository()
    {
        return $this->getRepository('User');
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryRepository()
    {
        return $this->getRepository('Category');
    }

    /**
     * @return ReviewRepository
     */
    public function getReviewRepository()
    {
        return $this->getRepository('Review');
    }
}