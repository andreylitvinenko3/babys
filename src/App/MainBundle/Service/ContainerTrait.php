<?php

namespace App\MainBundle\Service;

use App\MainBundle\Entity\User;
use App\MainBundle\Mail\MailSender;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;

trait ContainerTrait
{
    use ContainerAwareTrait;

    /**
     * @return FormFactory
     */
    public function getFormFactory()
    {
        return $this->getContainer()->get('form.factory');
    }

    public function getTokenStorage()
    {
        return $this->getContainer()->get('security.token_storage');
    }

    public function getAuthorizationChecker()
    {
        return $this->getContainer()->get('security.authorization_checker');
    }

    public function getAuthenticationUtils()
    {
        return $this->getContainer()->get('security.authentication_utils');
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if (null === $token = $this->getTokenStorage()->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->getContainer()->get('session');
    }

    public function addFlashMessage($type, $message)
    {
        return $this->getSession()->getFlashBag()->add($type, $message);
    }

    public function getWebDirectory()
    {
        return $this->getContainer()->getParameter('kernel.root_dir') . '/../web';
    }

    /**
     * @return MailSender
     */
    public function getMailSender()
    {
        return $this->getContainer()->get('app.mail_sender');
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->getContainer()->get('request');
    }

    /**
     * @return Paginator
     */
    public function getPaginator()
    {
        return $this->getContainer()->get('knp_paginator');
    }
}
