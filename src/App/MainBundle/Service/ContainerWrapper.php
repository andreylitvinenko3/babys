<?php

namespace App\MainBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ContainerWrapper
{
    use ContainerTrait;
    use DoctrineTrait;

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function get($id, $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE)
    {
        return $this->container->get($id, $invalidBehavior);
    }

    public function has($id)
    {
        return $this->container->has($id);
    }

    public function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    public function hasParameter($name)
    {
        return $this->container->hasParameter($name);
    }

    public function createNamedBuilder($name, $data = null, array $options = [])
    {
        return $this->container->get('form.factory')->createNamedBuilder($name, 'form', $data, $options);
    }
}