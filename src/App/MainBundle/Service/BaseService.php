<?php

namespace App\MainBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseService
{
    use ContainerTrait;

    /**
     * @var ContainerInterface $container
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }
}