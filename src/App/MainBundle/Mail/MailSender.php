<?php

namespace App\MainBundle\Mail;

class MailSender
{
    private $mailer;
    private $twig;
    private $host;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send($email, $template, $params = [], $attachments = [])
    {
        if (!$email) {
            return;
        }

        $emails = $email;
        if (!is_array($emails)) {
            $emails = [$email];
        }

        foreach ($emails as $email) {
            $message = $this->prepareMessage($email, $template, $params, $attachments);
            $this->mailer->send($message);
        }
    }

    public function prepareMessage($email, $template, $params = [], $attachments = [])
    {
        $params = array_merge([
            'host' => $this->host
        ], $params);
        /**
         * @var \Twig_Template $tpl
         */
        $tpl = $this->twig->loadTemplate('MainBundle:Mail:' . $template . '.html.twig');

        $message = new \Swift_Message();

        $message
            ->setSubject($tpl->renderBlock('subject', $params))
            ->setFrom('noreply@baby-clubs.ru')
            ->setTo($email)
            ->setBody($tpl->renderBlock('body', $params))
            ->setContentType('text/html')
        ;

        foreach ($attachments as $attach) {
            $message->attach($attach);
        }

        return $message;
    }
}
