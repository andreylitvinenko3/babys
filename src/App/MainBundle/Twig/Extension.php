<?php

namespace App\MainBundle\Twig;

use App\MainBundle\Entity\User;
use Sirian\Helpers\TextUtils;

class Extension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            'yesno' => new \Twig_SimpleFilter('yesno', [$this, 'yesNoFilter'], ['is_safe' => ['html']]),
            'pluralize' => new \Twig_SimpleFilter('pluralize', [$this, 'pluralize'])
        ];
    }

    public function getFunctions()
    {
        return [
            'roles' => new \Twig_SimpleFunction('roles', [$this, 'rolesFunction'])
        ];
    }

    public function rolesFunction($roles)
    {
        $result = [];
        foreach ((array)$roles as $role) {
            if (array_key_exists($role, User::$roleChoices)) {
                $result[] = User::$roleChoices[$role];
            }
        }

        return implode(', ', $result);
    }

    public function yesNoFilter($value)
    {
        $pattern = '<div class="label label-%s">%s</div>';
        if ($value) {
            $class = 'success';
            $value = 'Да';
        } else {
            $class = 'danger';
            $value = 'Нет';
        }

        return sprintf($pattern, $class, $value);
    }

    public function pluralize($count, $variants)
    {
        return TextUtils::pluralize($count, $variants);
    }

    public function getName()
    {
        return 'main';
    }
}
