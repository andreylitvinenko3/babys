<?php

namespace App\MainBundle\Filter;

abstract class BaseFilter
{
    const ALL = '';
    const YES = 'yes';
    const NO = 'no';

    protected static $yesNoChoices = [
        self::ALL => 'Все',
        self::YES => 'Да',
        self::NO => 'Нет'
    ];

    public static function getYesNoChoices()
    {
        return self::$yesNoChoices;
    }
}