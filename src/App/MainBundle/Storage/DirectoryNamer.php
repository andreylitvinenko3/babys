<?php

namespace App\MainBundle\Storage;

use Symfony\Component\DependencyInjection\Container;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class DirectoryNamer implements DirectoryNamerInterface
{
    public function directoryName($object, PropertyMapping $mapping)
    {
        $name = base_convert(sha1($object->getTitle()), 16, 36);
        $imageType = substr($mapping->getMappingName(), strpos($mapping->getMappingName(), '_') + 1);

        if (!$imageType) {
            throw new \LogicException('Image type is undefined');
        }

        $class = get_class($object);
        $baseClass = substr($class, strrpos($class, '\\') + 1);

        return sprintf('/%s/%s/%s/%s', Container::underscore($baseClass), $imageType, substr($name, 0, 2), substr($name, 2, 2));
    }
}