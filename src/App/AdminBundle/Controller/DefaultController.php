<?php

namespace App\AdminBundle\Controller;

class DefaultController extends BaseController
{
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }
}
