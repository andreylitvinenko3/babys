<?php

namespace App\AdminBundle\Controller;

use Knp\Component\Pager\Pagination\AbstractPagination;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends \App\MainBundle\Controller\BaseController
{
    public function createFilterFormBuilder($data = null, array $options = [])
    {
        $formBuilder = parent::createGETFormBuilder($data, [
            'required' => false
        ]);

        return $formBuilder;
    }

    /**
     * @param $target
     * @param int $limit
     * @param array $options
     * @return AbstractPagination
     */
    public function paginate($target, $limit = 50, array $options = array())
    {
        $r = $this->getRequest();

        $pagination = $this->getPaginator()
            ->paginate(
                $target,
                $r->query->get('page', $r->request->get('page', 1)),
                $r->query->get('limit', $r->request->get('limit', $limit)),
                $options
            );

        $pagination->setTemplate('AdminBundle:Pagination:panel_foot.html.twig');
        $pagination->setSortableTemplate('AdminBundle:Pagination:sortable_link.html.twig');

        return $pagination;
    }

    protected function renderJson($data)
    {
        return new Response(json_encode($data));
    }
}