<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Form\Type\ReviewType;
use App\MainBundle\Entity\Review;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReviewController extends BaseController
{
    public function indexAction(Request $request)
    {
        $qb = $this
            ->getReviewRepository()
            ->createQueryBuilder('r')
            ->orderBy('r.created', 'desc')
        ;

        $pagination = $this->paginate($qb);

        return $this->render('AdminBundle:Review:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function newAction(Request $request)
    {
        $review = new Review();

        return $this->edit($request, $review);
    }

    public function editAction(Request $request, $id)
    {
        $review = $this->findReview($id);

        return $this->edit($request, $review);
    }

    public function deleteAction(Request $request, $id)
    {
        $review = $this->findReview($id);

        $em = $this->getEntityManager();
        $em->remove($review);
        $em->flush();

        return $this->redirectToRoute('admin_review');
    }

    /**
     * @param $id
     * @return Review
     */
    private function findReview($id)
    {
        $review = $this->getReviewRepository()->find($id);

        if (!$review) {
            throw new NotFoundHttpException();
        }

        return $review;
    }

    private function edit(Request $request, Review $review)
    {
        $isNew = !$review->getId();

        $form = $this->createForm(new ReviewType(), $review);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getEntityManager();

            $em->persist($review);
            $em->flush();

            $this->addFlashMessage('success', sprintf('Обзор успешно %s', $isNew ? 'добавлен' : 'обновлен'));

            return $this->redirectToRoute('admin_review_edit', ['id' => $review->getId()]);
        }

        return $this->render('AdminBundle:Review:edit.html.twig', [
            'isNew' => $isNew,
            'form' => $form->createView(),
            'review' => $review
        ]);
    }
}