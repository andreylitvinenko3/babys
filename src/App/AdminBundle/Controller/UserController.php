<?php

namespace App\AdminBundle\Controller;

use App\MainBundle\Entity\User;
use App\MainBundle\Filter\UserFilter;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends BaseController
{
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('admin_user_list'));
    }

    public function listAction(Request $request)
    {
        $filter = new UserFilter();
        $form = $this->createFilterFormBuilder($filter)
            ->add('email', 'text', [
                'label' => 'E-mail',
                'required' => false
            ])
            ->add('roles', 'choice', [
                'choices' => User::$roleChoices,
                'required' => false,
                'multiple' => true,
                'label' => 'Роль'
            ])
            ->add('submit', 'submit', [
                'label' => 'Искать'
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        $qb = $this->getUserRepository()->createFilteredQueryBuilder($filter);
        $pagination = $this->paginate($qb);

        return $this->render('AdminBundle:User:list.html.twig', [
            'pagination' => $pagination,
            'filterForm' => $form->createView()
        ]);
    }

    public function editAction($id = null)
    {
        $isNew = null === $id;

        if ($isNew) {
            $entity = new User();
        } else {
            $entity = $this->findUser($id);
        }

        $builder = $this->createFormBuilder($entity)
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('plainPassword', 'text', [
                'label' => 'Пароль',
                'required' => false
            ])
            ->add('roles', 'choice', [
                'label' => 'Роли',
                'choices' => User::$roleChoices,
                'multiple' => true
            ])
            ->add('submit', 'submit', [
                'label' => $isNew ? 'Добавить' : 'Сохранить'
            ])
        ;

        $editForm = $builder->getForm();
        $editForm->handleRequest($this->getRequest());

        if ($editForm->isValid()) {

            $entity->setUsername($entity->getEmail());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            if ($isNew) {
                $this->addFlashMessage('success', 'Пользователь создан');
            } else {
                $this->addFlashMessage('success', 'Пользователь сохранен');
            }

            return $this->redirect($this->generateUrl('admin_user_edit', [
                'id' => $entity->getId()
            ]));
        }

        return $this->render('AdminBundle:User:edit.html.twig', [
            'isNew' => $isNew,
            'entity' => $entity,
            'form'   => $editForm->createView()
        ]);
    }

    public function deleteAction($id)
    {
        if (!$this->getAuthorizationChecker()->isGranted(User::ROLE_SUPERADMIN)) {
            return $this->createAccessDeniedException();
        }

        $user = $this->findUser($id);

        $dm = $this->getDoctrine()->getManager();
        $dm->remove($user);
        $dm->flush();

        $this->addFlashMessage('success', 'Пользователь удален');
        return $this->redirect($this->generateUrl('admin_user_list'));
    }

    /**
     * @param $id
     * @return null|User
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function findUser($id)
    {
        $entity = $this->getUserRepository()->find($id);

        if (!$entity) {
            throw new NotFoundHttpException();
        }

        return $entity;
    }
} 