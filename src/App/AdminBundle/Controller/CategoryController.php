<?php

namespace App\AdminBundle\Controller;

use App\MainBundle\Entity\Category;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends BaseController
{
    public function moveAction(Request $request)
    {
        $post = $request->request;

        $id = $post->get('id');
        $parentId = $post->get('parent_id');
        $moveCount = (int)$post->get('after_count');

        $repo = $this->getCategoryRepository();
        /**
         * @var Category $item
         */
        $item = $repo->find($id);

        if (!$item) {
            throw new NotFoundHttpException('Item not found');
        }

        $em = $this->getDoctrine()->getManager();

        if ($parentId) {
            /**
             * @var Category $parent
             */
            $parent = $repo->find($parentId);
        } else {
            $parent = $repo->getFakeParent();
        }

        $item->setParent($parent);

        $em->flush();

        $repo->moveDown($item, true);
        $em->flush();

        if ($moveCount > 0) {
            $repo->moveUp($item, $moveCount);
            $em->flush();
        }

        return new JsonResponse([]);
    }

    public function indexAction()
    {
        return $this->render('AdminBundle:Category:index.html.twig', [
            'root' => $this->getCategoryRepository()->getTree()
        ]);
    }

    public function deleteAction(Request $request, $id)
    {
        $result = ['result' => 'ok'];

        if ($request->isMethod('POST') || $request->isMethod('DELETE')) {
            $em = $this->getEntityManager();
            $entity = $this->getCategoryRepository()->find($id);
            if ($entity) {
                $em->remove($entity);
                $em->flush();
            }
        } else {
            $result = ['error' => 'Неверный метод'];
        }

        return new JsonResponse($result);
    }

    public function newAction(Request $request)
    {
        $parent = $this->findCategory($request->query->get('parentId'));

        $category = new Category();

        if ($parent->getId()) {
            $category->setParent($parent);
        }

        return $this->edit($request, $category);
    }

    public function editAction(Request $request, $id)
    {
        $category = $this->findCategory($id);

        return $this->edit($request, $category);
    }

    protected function edit(Request $request, Category $category)
    {
        $isNew = !$category->getId();

        $builder = $this->createFormBuilder($category);

        $this->buildForm($builder);

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getCategoryRepository()->edit($category);

            $this->addFlashMessage('success', 'Категория успешно ' . ($isNew ? 'добавлен' : 'обновлен'));

            return $this->redirect($this->generateUrl('admin_category_edit', ['id' => $category->getId()]));
        }

        return $this->render('AdminBundle:Category:edit.html.twig', [
            'isNew' => $isNew,
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    protected function buildForm(FormBuilderInterface $builder)
    {
        $queryBuilder = $this->getCategoryRepository()->getNodesHierarchyQueryBuilder();

        /**
         * @var Category $entity
         */
        $entity = $builder->getData();
        if ($entity->getId()) {
            $queryBuilder
                ->andWhere('node.id != :id')
                ->setParameter('id', $entity->getId());
        }

        $builder
            ->add('parent', 'entity', [
                'required' => false,
                'class' => 'App\MainBundle\Entity\Category',
                'empty_value' => '---',
                'property' => 'shiftedTitle',
                'label' =>  'Родитель',
                'query_builder' => $queryBuilder
            ])
            ->add('title', 'text', [
                'label' => 'Название'
            ])
            ->add('shortTitle', 'text', [
                'label' => 'Короткое название (в блоке фильтров)'
            ])
            ->add('alias', 'text', [
                'label' => 'Алиас'
            ])
            ->add('submit', 'submit', [
                'label' => $entity->getId() ? 'Сохранить' : 'Добавить',
            ])
        ;
    }

    /**
     * @param $id
     * @return null|Category
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function findCategory($id = null)
    {
        if ($id) {
            $category = $this->getCategoryRepository()->find($id);

            if (!$category) {
                throw new NotFoundHttpException();
            }
        } else {
            $category = new Category();
        }

        return $category;
    }
}
