<?php

namespace App\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', null, [
                'label' => 'Автор'
            ])
            ->add('text', 'textarea', [
                'label' => 'Текст'
            ])
            ->add('imageFile', 'file', [
                'label' => 'Фотография',
                'hint' => 'Размеры: ',
                'required' => false
            ])
            ->add('enabled', 'checkbox', [
                'label' => 'Показывать на сайте',
                'required' => false
            ])
            ->add('submit', 'submit', [
                'label' => 'Сохранить'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\MainBundle\Entity\Review'
        ]);
    }

    public function getName()
    {
        return 'admin_review';
    }

}